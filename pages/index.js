import React, { Component } from 'react'
import { withRouter } from 'next/router'
import axios from 'axios'

// Components
import Head from '../components/head'

class Weather extends Component {
  static async getInitialProps({ query }) {
    if (query.city != null) {
      return await axios
        .get(`http://api.openweathermap.org/data/2.5/weather?q=${query.city}&units=metric&appid=166d00e26d3ff2c6149e89feccc5c59a`)
        .then((response) => {
          const weatherData = response.data

          return {
            city: weatherData.name,
            temperature: weatherData.main.temp,
            humidity: weatherData.main.humidity,
            wind: weatherData.wind.speed,
            windDirectionDegrees: weatherData.wind.deg
          }
        })
        .catch((error) => {
          return {
            error: error.message,
            city: query.city
          }
        })
    } else {
      return {}
    }
  }

  state = {
    city: this.props.city,
    temperature: this.props.temperature,
    humidity: this.props.humidity,
    wind: this.props.wind,
    windDirectionDegrees: this.props.windDirectionDegrees,
    searchField: '',
    error: this.props.error
  }

  // = Handlers =======================================
  handleSearchChange = ({ target }) => {
    return this.setState({ searchField: target.value })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    event.stopPropagation()

    return axios
      .get(`http://api.openweathermap.org/data/2.5/weather?q=${this.state.searchField}&units=metric&appid=166d00e26d3ff2c6149e89feccc5c59a`)
      .then((response) => {
        const weatherData = response.data

        window.history.replaceState(null, null, `?city=${this.state.searchField}`)
        this.setState({
          city: weatherData.name,
          temperature: weatherData.main.temp,
          humidity: weatherData.main.humidity,
          wind: weatherData.wind.speed,
          windDirectionDegrees: weatherData.wind.deg,
          error: null
        })
      })
      .catch((error) => {
        window.history.replaceState(null, null, `?city=${this.state.searchField}`)
        this.setState({ error: error.message, city: this.state.searchField })
      })
  }

  // = Generators =====================================

  // = Getters ========================================
  get windDirection() {
    if (this.state.windDirectionDegrees >= 45 && this.state.windDirectionDegrees < 135) {
      return 'East'
    } else if (this.state.windDirectionDegrees >= 135 && this.state.windDirectionDegrees < 225) {
      return 'South'
    } else if (this.state.windDirectionDegrees >= 225 && this.state.windDirectionDegrees < 315) {
      return 'West'
    } else {
      return 'North'
    }
  }

  get urlCityParam() {
    let params
    if (process.browser) {
      params = new URLSearchParams(window.location.search)
    } else {
      params = new URLSearchParams(this.props.router.query)
    }
    const city = params.get('city')

    return city || ''
  }

  // = Lifecycle ======================================

  render() {
    if (this.state.error == null && this.urlCityParam.length === 0) {
      return (
        <section>
          <Head />
          <div className="widget" style={{ margin: '10px', width: '300px' }}>
            <div className="panel panel-info">
              <div className="panel-heading">Put in a location to see the weather</div>
              <ul className="list-group">
                <li className="list-group-item">
                  <form className="form-inline" action="/" method="GET" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <input type="text" className="form-control" id="city" name="city" placeholder="City" value={this.state.searchField} onChange={this.handleSearchChange} />
                    </div>
                    <button type="submit" className="btn btn-default">
                      Search
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </section>
      )
    }

    if (this.state.error != null) {
      return (
        <section>
          <Head title="Error" description={`Error: ${this.state.city} not found!`} />
          <div className="widget" style={{ margin: '10px', width: '300px' }}>
            <div className="panel panel-danger">
              <div className="panel-heading">
                Error: <b>{this.state.city}</b> not found!
              </div>
              <ul className="list-group">
                <li className="list-group-item">
                  <form className="form-inline" action="/" method="GET" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <input type="text" className="form-control" id="city" name="city" placeholder="City" value={this.state.searchField} onChange={this.handleSearchChange} />
                    </div>
                    <button type="submit" className="btn btn-default">
                      Search
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </section>
      )
    }

    return (
      <section>
        <Head title="Weather" query={this.props.router.asPath} />

        <div className="widget" style={{ margin: '10px', width: '300px' }}>
          <div className="panel panel-info">
            <div className="panel-heading">
              Weather in <b>{this.state.city}</b>
            </div>
            <ul className="list-group">
              <li className="list-group-item">
                Temperature: <b>{this.state.temperature}°C</b>
              </li>
              <li className="list-group-item">
                Humidity: <b>{this.state.humidity}</b>
              </li>
              <li className="list-group-item">
                Wind:{' '}
                <b>
                  {this.state.wind} m/s {this.windDirection}
                </b>
              </li>
              <li className="list-group-item">
                <form className="form-inline" action="/" method="GET" onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <input type="text" className="form-control" id="city" name="city" placeholder="City" value={this.state.searchField} onChange={this.handleSearchChange} />
                  </div>
                  <button type="submit" className="btn btn-default">
                    Search
                  </button>
                </form>
              </li>
            </ul>
          </div>
        </div>
      </section>
    )
  }
}

export default withRouter(Weather)
