FROM node:lts-alpine

ENV DIR=/app/conlan
WORKDIR $DIR

COPY package.json yarn.lock ./

RUN yarn install --pure-lockfile --frozen-lockfile --non-interactive

COPY . $DIR

RUN yarn run build

EXPOSE 3000

CMD yarn run start
